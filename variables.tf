variable "ssh_user" {
  type = string
  default = "root"
}

variable "ssh_host" {
  type = string
  default = "51.15.254.222"
}
