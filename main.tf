resource "null_resource" "ssh_target" {
  connection {
    type        = "ssh"
    user        = var.ssh_user
    host        = var.ssh_host
    private_key = "${file("/root/.ssh/id_rsa_terraform")}"
  }

    provisioner "file" {
    source      = "docker-compose.yml"
    destination = "docker-compose.yml"
  }
  provisioner "remote-exec" {
    inline = [
        "docker container stop $(docker container ls -aq)",
        "sudo docker-compose up  -d"
    ]
  }
}
